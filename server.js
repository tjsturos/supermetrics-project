const http = require('http');
const fs = require('fs');
const path = require('path');

const port = process.env.PORT || 3000;

const supermetrics = require('./helpers/supermetrics-api');
const stats = require('./helpers/stats');

http.createServer(async function (request, response) {
  const filePath = '.' + request.url;
  
  if (filePath == './') {
    try {
      let posts = await supermetrics.getAllPosts();
      const postStatistics = stats.getStatsFromPosts(posts);
      response.writeHead(200, { 'Content-Type': 'application/json' });
      const content = JSON.stringify(postStatistics);
      response.end(content, 'utf-8');
    } catch (error) {
      console.log(error)
      response.writeHead(500, { 'Content-Type': 'application/json' });
      response.end(JSON.stringify({
        error: "Oops! Something went wrong!"
      }));
    }
    
  } else {
    // always redirect to index page
    response.writeHead(302, {
      'Location': '/'
    });
    response.end();
  }
  
}).listen(port, () => {
  console.log(`The server is now running on localhost:${port}.`)
});

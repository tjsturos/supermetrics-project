# Supermetrics Assignment
The goal of this project is to access Supermetric's Assignment API endpoint and return
processed data analytics, in Javascript. As per the assignment:
**"Fetch and manipulate JSON data from a fictional Supermetrics Social Network REST API."**

----

[[_TOC_]]

----  
## Installing the Necessities

  1. Have a running version of node.js installed.
     - Install via the [Nodejs.org](https://nodejs.org/en/download/) site.

  2. Make sure you have a package manager, yarn or npm.
    - Npm should already be installed with Nodejs. 
    - Yarn can be installed from [their website](https://yarnpkg.com/). `yarn`
  
  3. Download the project files using 
  [git clone](https://www.git-scm.com/docs/git-clone) terminal command or download the file 
  archive directly and unzip to a local directory.
     - [Download the archive (zip)](https://gitlab.com/tjsturos/supermetrics-project/-/archive/master/supermetrics-project-master.zip)  
     - Clone via Git SSH: `git clone git@gitlab.com:tjsturos/supermetrics-project.git`
     - Clone via Git HTTPS: `git clone https://gitlab.com/tjsturos/supermetrics-project.git`
----  
## Running the application

There are two options, locally or if you deploy on a remote server.

In either case you can run the commands `start:dev` or `start:prod`
with the difference being where the configuration variables are
originating from.  

`start:dev` uses a 'config.json' file that should be located in the root
directory, while `start:prod` is intended to be used in production and
is looking for the variables to be set in a CI/CD pipeline, although they
can be set manually in the `package.json` scripts.

E.G. `"start:prod": "SUPERMETRICS_REGISTER_CLIENT_ID:... node server.js"`

### Locally
  1. In a terminal, change the terminal to the projects directory. 
  2. Install the required packages.  
    - Depending on the package manager you have: `yarn` or `npm install`
  3. Create a file named 'config.json' and input your credentials as follows:
  ```json
    {
      "supermetrics": {
        "register": {
          "email" : "<you-email>",
          "client_id" : "<your-client-id>",
          "name": "<your-name>"
        }
      }
    }
  ``` 
  4. In the terminal type `yarn start:dev` or `npm run start:dev`.
  5. In your browser navigate to [localhost:3000](localhost:3000).

  ### Remotely
   1. If remotely deploying, once you have it set up like a local environment (minus the config.json file)
   2. Set the following environment variables either in your CI/CD env or by modifying the start scripts to include these:
```js
      SUPERMETRICS_REGISTER_CLIENT_ID = '<client-id>'
      SUPERMETRICS_REGISTER_NAME = '<your-name>'
      SUPERMETRICS_REGISTER_EMAIL = '<your-email>'
```
  3. Run `yarn start:prod` or `npm run start:prod`.
  4. Set the server to expose port 3000
  5. Navigate to the url of your server: '<server-address>:3000'
----  
## Application data structure
See the openapi.yaml.

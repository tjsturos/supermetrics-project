const moment = require('moment');

/**
 * 
 * Takes the posts and gets the following statistics from them:  
 *  - Average character length of a post / month
 *  - Longest post by character length / month
 *  - Total posts split by week
 *  - Number of posts per user / month
 *  - Average number of posts per user / month
 *  - Total posts / month
 * 
 * @param {Object} postsByPage - object containing posts by pageId
 * @returns {Object} statistics for all the posts per the description above
 */
function getStatsFromPosts(postsByPage) {
  
  const stats = {
    postsByWeek: {},
    statsByMonth: {}
  };

  for (const pageId in postsByPage) {
    const page = postsByPage[pageId]; 
    // for each post update the stats object
    for (postId in page) {
      const post = page[postId];
      const date = moment(post.created_time);
      const year = date.year();
      const month = date.month();

      const week = date.week();
      updatePostCountForWeek(stats.postsByWeek, year, week);

      initializeMonthOnStatsObj(stats.statsByMonth, year, month);
      updatePostCountForMonth(stats.statsByMonth, year, month);

      const postLength = post.message.length;
      updateCharacterCountForMonth(stats.statsByMonth, year, month, postLength);
      updateAveragePostCharacterLength(stats.statsByMonth, year, month);

      updateLongestMonthlyPost(stats.statsByMonth, year, month, postLength);

      const user = post.from_id;
      updateUsersPostsPerMonth(stats.statsByMonth, year, month, user);
    }
  };
  averageUserPostsPerMonth(stats.statsByMonth);
  return stats;
}

/**
 * Each month has it's own stats, so this creates the base stats
 * for each month (in appropriate year) in the stats object.
 * 
 * @param {Object} statsObj - the object containing the stats 
 * @param {String|Number} year - the year that a post was created in
 * @param {String|Number} month - the month a post was created in
 */
function initializeMonthOnStatsObj(statsObj, year, month) {
  if (!(year in statsObj)) {
    statsObj[year] = {}
  }

  if (!(month in statsObj[year])) {
    statsObj[year][month] = {
      postsPerUser: {},
      totalPosts: 0,
      totalCharacters: 0,
      averagePostLength: 0,
      longestPostLength: 0
    }
  }
}

/**
 * Add another post to the appropriate week.
 * 
 * @param {Object} statsObj - the stats object to update
 * @param {String|Number} year - the year the post was created in
 * @param {String|Number} week - the week number the post was created in
 */
function updatePostCountForWeek(statsObj, year, week) {
  if (!(year in statsObj)) {
    statsObj[year] = {};
  }
  if (!(week in statsObj[year])) {
    statsObj[year][week] = 1;
  } else {
    statsObj[year][week] += 1;
  }
}

/**
 * Update the total number of posts for each post.
 * 
 * @param {Object} statsObj - the object containing the stats 
 * @param {String|Number} year - year the post was created in
 * @param {String|Number} month - the month the post was created in
 **/
function updatePostCountForMonth(statsObj, year, month) {
  statsObj[year][month].totalPosts += 1;
}

/**
 * Update the total number of characters from all posts this month.
 * 
 * @param {Object} statsObj - the object containing the stats 
 * @param {String|Number} year - year the post was created in
 * @param {String|Number} month - the month the post was created in
 * @param {Number} postLength - number of characters in post
 */
function updateCharacterCountForMonth(statsObj, year, month, postLength) {
  statsObj[year][month].totalCharacters += postLength;
}

/**
 * Update the average post length (number of characters) in a given month.
 * 
 * @param {Object} statsObj 
 * @param {String|Number} year 
 * @param {String|Number} month 
 */
function updateAveragePostCharacterLength(statsObj, year, month) {
  const totalPosts = statsObj[year][month].totalPosts;
  const totalChars = statsObj[year][month].totalCharacters;
  statsObj[year][month].averagePostLength = totalChars / totalPosts;
}

/**
 * Update the longest monthly post, if post length is longer.
 *  
 * @param {Object} statsObj - the object storing the stats
 * @param {Number|String} year - the year the post was created
 * @param {Number|String} month - the month of the year the post was created
 * @param {Number} postLength - the number of characters in a post 
 */
function updateLongestMonthlyPost(statsObj, year, month, postLength) {
  if (statsObj[year][month].longestPostLength < postLength) {
    statsObj[year][month].longestPostLength = postLength;
  }
}

/**
 * Update the number of posts by each user in the given year and month.
 * 
 * @param {Object} statsObj 
 * @param {Number|String} year 
 * @param {Number|String} month 
 * @param {String} user 
 */
function updateUsersPostsPerMonth(statsObj, year, month, user) {
  if (!(user in statsObj[year][month].postsPerUser)) {
    statsObj[year][month].postsPerUser[user] = 0;
  }
  statsObj[year][month].postsPerUser[user] += 1;
}

/**
 * Sets the average number of posts per user / month.
 * 
 * Note: "Average number of posts per user / month" is kind of vague,
 * I've defined it as "Only using all users who posted in the month, their average
 * number of posts is: X".  This is because I have no idea on if there are users
 * who have not posted, or if this is asking to average the number each users
 * posts across all 6 months. 
 * 
 * The calculations would vary:
 * 
 * If there are users who have not posted this month, then the average would go
 * down (add user to count (demoninator), plus 0 to post counts (numerator)).
 * 
 * Or if you are just averaging the stats per user, then you need to find all the post
 * for a user during this timeframe and divide by 6.  You could then further this by
 * averageing this by adding each user's average together and diving by the number of users
 * who posted.
 * 
 * Or as seen in the calculations below, I just take the number of posts that month
 * divided by the number of users who have posted.
 * 
 * @param {Object} statsObj 
 */
function averageUserPostsPerMonth(statsObj) {
  for (const yearId in statsObj) {
    if (statsObj.hasOwnProperty(yearId)) {
      const months = statsObj[yearId];
      for (const monthId in months) {
        if (months.hasOwnProperty(monthId)) {
          const monthlyStats = months[monthId];
          const userCount = Object.keys(monthlyStats.postsPerUser).length;
          const totalPosts = monthlyStats.totalPosts;

          statsObj[yearId][monthId].averagePostsPerUser = totalPosts / userCount;
        }
      }
    }
  }
}

exports.getStatsFromPosts = getStatsFromPosts;
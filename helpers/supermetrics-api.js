const fs = require('fs');
const path = require('path');
const axios = require('axios');

function getRegisterCredentials() {
  try {
    let credentials;

    if (process.env.NODE_ENV === 'development') {
      let configFile = fs.readFileSync('./config.json', 'UTF-8');
      credentials = JSON.parse(configFile).supermetrics.register;
    } else {
      credentials = {
        'client_id': process.env.SUPERMETRICS_REGISTER_CLIENT_ID,
        'name': process.env.SUPERMETRICS_REGISTER_NAME,
        'email': process.env.SUPERMETRICS_REGISTER_EMAIL
      }
    }
    return credentials;
  } catch (error) {
    console.log(error);
  }
}


function registerOnSuperMetrics() {
  return new Promise(async (resolve, reject) => {
    try {
      const registerUrl = "https://api.supermetrics.com/assignment/register";
      const credentials = getRegisterCredentials();
      const response = await axios.post(registerUrl, credentials);
      resolve(response.data.data.sl_token);
    } catch (error) {
      console.log(error);
      reject("Could not register on Supermetrics assignment endpoint!");
    }
  });
  
}

async function getSuperMetricsPosts(slToken, page = 1) {
  return new Promise(async (resolve, reject) => {
    const postsUrl = `https://api.supermetrics.com/assignment/posts?sl_token=${slToken}&page=${page}`;
    try {
      const response = await axios.get(postsUrl);
      resolve(response.data.data.posts);
    } catch (error) {
      console.log(error);
      reject(`Could not get posts for page ${page}!`);
    }
  });
}

async function getAllPosts() {
  return new Promise(async (resolve, reject) => {
    const pageQuantity = 10;
    const postPages = {};
    
    try {
      const token = await registerOnSuperMetrics();
      for (let page = 1; page <= pageQuantity; page++) {
        let postsByPage = await getSuperMetricsPosts(token, page);
        postPages[page] = postsByPage;
      }
      resolve(postPages);
    } catch (error) {
      console.log(error);
      reject("Couldn't get all posts!");
    }
  });
}


exports.getToken = registerOnSuperMetrics;
exports.getPostsByPage = getSuperMetricsPosts;
exports.getAllPosts = getAllPosts;

